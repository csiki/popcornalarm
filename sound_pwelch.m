clc;clear all;close all;
%%
% [s,fs] = audioread('Popcorn_Popping_In_Microwave.wav');
% y = mean(s,2);
% [pxx,f] = pwelch(y, [], [], fs/2, fs);
% plot(f, log(pxx));

% difference of first compared to second half spectrum
% [pxx1,f] = pwelch(y(1:length(y)/2), [], [], fs/2, fs);
% [pxx2,f] = pwelch(y(length(y)/2:end), [], [], fs/2, fs);
% plot(f, log(pxx2) - log(pxx1));

% OptiPop
% from 7000Hz, 11000Hz
% low pass filter
[sound, Fs] = audioread('Popcorn_Popping_In_Microwave.wav');
Fn = Fs/2;                                  % Nyquist Frequency
Fco = 10000;                                % Passband (Cutoff) Frequency
Fsb = 11000;                                % Stopband Frequency
Rp = 1;                                     % Passband Ripple (dB)
Rs = 10;                                    % Stopband Ripple (dB)
[n,Wn] = buttord(Fco/Fn, Fsb/Fn, Rp, Rs);   % Filter Order & Wco
[b,a] = butter(n, Wn, 'low');               % Lowpass Is Default Design
[sos,g] = tf2sos(b,a);                      % Second-Order-Section For STability
freqz(sos, 2048, Fs)                        % Check Filter Performance
low_passed_sound = filtfilt(sos, g, sound);

Fn = Fs/2;                                  % Nyquist Frequency
Fco = 7000;                                 % Passband (Cutoff) Frequency
Fsb = 6000;                                 % Stopband Frequency
Rp = 1;                                     % Passband Ripple (dB)
Rs = 10;                                    % Stopband Ripple (dB)
[n,Wn] = buttord(Fco/Fn, Fsb/Fn, Rp, Rs);   % Filter Order & Wco
[b,a] = butter(n, Wn, 'high');               % Lowpass Is Default Design
[sos,g] = tf2sos(b,a);                      % Second-Order-Section For STability
freqz(sos, 2048, Fs)                        % Check Filter Performance
filtered_sound = filtfilt(sos, g, low_passed_sound);
%soundsc(filtered_sound);