close all; clear all;
%% OptiPop
PLOTS = 1;
min_possible_freq = 5000;
max_possible_freq = 20000;
freq_window_len_sec = 10;
under_threshold_certainty_time = 5; % in sec
ultimate_pop_threshold = 0.5; % pop/sec
pop_bandwidth = 1000;
pop_variance_multiplier = 10; % TODO make classified sound plot
[sound, Fs] = audioread('Sound_Effect_-_Microwave_Popcorn.wav');
mono_sound = mean(sound,2);
mono_sound = [zeros(30*Fs, 1); mono_sound];

%% find frequency band of popping
index_first_part = 10*Fs;
sound_first_part = mono_sound(10*Fs:20*Fs); % no pop sounds
sound_last_part = mono_sound(80*Fs:90*Fs); % pop sounds
% sound_first_part = mono_sound(28*Fs:38*Fs); % no pop sounds
% sound_last_part = mono_sound(10*Fs:20*Fs); % pop sounds
sound_first_part = [sound_first_part; ...
    zeros(length(sound_last_part) - length(sound_first_part), 1)];
slen2 = round(length(mono_sound)/2);
slen2_fp = round(length(sound_first_part)/2);
slen2_lp = round(length(sound_last_part)/2);
f = ((0:slen2 - 1) / slen2) * Fs/2;
f_fp = ((0:slen2_fp - 1) / slen2_fp) * Fs/2-1;
f_lp = ((0:slen2_lp - 1) / slen2_lp) * Fs/2-1;
% run fft
sound_f = real(fft(mono_sound));
sound_f = abs(sound_f(1:length(sound_f)/2)); % half of it is essential
sound_f_first_part = real(fft(sound_first_part));
sound_f_first_part = abs(sound_f_first_part(1:round(length(sound_f_first_part)/2)));
sound_f_last_part = real(fft(sound_last_part));
sound_f_last_part = abs(sound_f_last_part(1:round(length(sound_f_last_part)/2)));
% filter relevant frequencies
sound_f = sound_f(f > min_possible_freq & f < max_possible_freq);
sound_f_first_part = sound_f_first_part(f_fp > min_possible_freq & f_fp < max_possible_freq);
sound_f_last_part = sound_f_last_part(f_lp > min_possible_freq & f_lp < max_possible_freq);
f = f(f > min_possible_freq & f < max_possible_freq);
f_fp = f_fp(f_fp > min_possible_freq & f_fp < max_possible_freq);
f_lp = f_lp(f_lp > min_possible_freq & f_lp < max_possible_freq);
% calculate spectrum difference
sound_f_smoothed = smooth((sound_f_last_part - mean(sound_f_last_part)) ./ (sound_f_first_part - mean(sound_f_first_part)), 0.05);
f_smoothed = smooth(f_fp, 0.05);
if PLOTS == 1
    subplot(3, 1, 1);
    plot(f_fp, smooth(sound_f_first_part, 0.05));
    subplot(3, 1, 2);
    plot(f_lp, smooth(sound_f_last_part, 0.05));
    subplot(3, 1, 3);
    plot(f_smoothed, sound_f_smoothed);
end
% determine pop sound freq
[m, i] = max(sound_f_smoothed);
pop_sound_freq = f_smoothed(i);

%% low pass filter
Fn = Fs/2;                                  % Nyquist Frequency
Fco = pop_sound_freq + pop_bandwidth/2;                                % Passband (Cutoff) Frequency
Fsb = pop_sound_freq + pop_bandwidth;                                % Stopband Frequency
Rp = 1;                                     % Passband Ripple (dB)
Rs = 10;                                    % Stopband Ripple (dB)
[n,Wn] = buttord(Fco/Fn, Fsb/Fn, Rp, Rs);   % Filter Order & Wco
[b,a] = butter(n, Wn, 'low');               % Lowpass
[sos,g] = tf2sos(b,a);                      % Second-Order-Section For STability
%freqz(sos, 2048, Fs)                        % Check Filter Performance
low_passed_sound = filtfilt(sos, g, sound);
%% high pass filter
Fn = Fs/2;                                  % Nyquist Frequency
Fco = pop_sound_freq - pop_bandwidth/2;                                 % Passband (Cutoff) Frequency
Fsb = pop_sound_freq - pop_bandwidth;                                 % Stopband Frequency
Rp = 1;                                     % Passband Ripple (dB)
Rs = 10;                                    % Stopband Ripple (dB)
[n,Wn] = buttord(Fco/Fn, Fsb/Fn, Rp, Rs);   % Filter Order & Wco
[b,a] = butter(n, Wn, 'high');               % Lowpass Is Default Design
[sos,g] = tf2sos(b,a);                      % Second-Order-Section For STability
%freqz(sos, 2048, Fs)                        % Check Filter Performance
filtered_sound = filtfilt(sos, g, low_passed_sound);
mono_filtered = mean(filtered_sound, 2);
sound_len = length(mono_filtered)/Fs;

%% plot and check filtered sound
% soundsc(filtered_sound);
if PLOTS == 1
	figure, plot(linspace(0, sound_len, length(mono_filtered)), mono_filtered);
    title('Filtered popcorn popping sound');
    xlabel('Time (sec)');
    ylabel('Amplitude');
end

%% windowing and calculate #pops
mono_positive = abs(mono_filtered);
window_len = round(0.05 * Fs);
mono_positive_extended = [mono_positive; zeros(window_len - mod(length(mono_positive), window_len), 1)];
mono_positive_epoched = reshape(mono_positive_extended, window_len, []);
windowed_means = mean(mono_positive_epoched);
pops = windowed_means > mean(mono_positive) + pop_variance_multiplier * var(mono_positive);
npop = sum(pops);
if PLOTS == 1
    figure, plot(windowed_means, 'b-'), hold on;
    plot((pops * 2 - 1) * max(windowed_means) / 2, 'r*'), ylim([0, max(windowed_means) + var(windowed_means)]), hold off;
end
%% calculate frequency of each n seconds window with (n-1)/n overlap
freq_window_len = round(freq_window_len_sec * Fs / window_len);
l = length(pops);
m = l - freq_window_len + 1;
windowed_pops = pops(hankel(1:m, m:l)); % rolling window
pop_frequency = sum(windowed_pops, 2);

%% determine point of stop
% TODO save time taken for popcorn to finish and average it accross popcorns
average_time_to_pop = 120; % in sec
optipop_slider = 0.1; % [0,1], user adjustable slider - the higher the more burnt
pops_so_far = 0;
under_threshold_for = 0; % time since
stop_time = 1;
for i = 1:length(pop_frequency)
    pops_so_far = pops_so_far + pop_frequency(i) / 90;
    max_so_far = max(pop_frequency(1:i));
    mean_so_far = mean(pop_frequency(1:i));
    if i/16 > average_time_to_pop*3/4 ... % ultimate too low
            && pop_frequency(i) < ultimate_pop_threshold * freq_window_len_sec
        stop_time = i;
        break;
    elseif i/16 > average_time_to_pop*3/4 ...
            && pop_frequency(i) < (mean_so_far + (max_so_far - mean_so_far) * (1-optipop_slider))
        under_threshold_for = under_threshold_for + 1;
        if under_threshold_for > under_threshold_certainty_time * 16
            stop_time = i;
            break;
        end
    else
        under_threshold_for = 0;
    end
end

%% plot pop frequency
if PLOTS == 1
    figure, plot(pop_frequency), hold on;
    title('Pop frequency');
    xlabel('Time (rolling window)');
    ylabel('Number of pops in block');
    plot(stop_time, pop_frequency(stop_time), 'r*'), hold off;
end







